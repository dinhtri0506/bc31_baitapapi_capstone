import { snackBar } from "../../Library/js-snackbar-master/dist/js-snackbar.js";

export const NOTICE_CONTROLLERS = {
  toastNotice: (cart, id) => {
    let index = cart.findIndex((item) => {
      return item.id * 1 === id;
    });
    let name = cart[index].name;

    new snackBar({
      message: `${name} has been added in your cart`,
      timeout: 3000,
      position: "bl",
      fixed: true,
    });
  },
  purchase: () => {
    new snackBar({
      message: "Purchase success",
      timeout: 3000,
      position: "tm",
      fixed: true,
      icon: " ",
      status: "success",
    });
  },
};
