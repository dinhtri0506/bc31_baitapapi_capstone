const CART = "CART";

export const CART_CONTROLLERS = {
  updateLocalStorage: (cart) => {
    let cartList = JSON.stringify(cart);
    localStorage.setItem(CART, cartList);

    CART_CONTROLLERS.renderCart(cart);
  },

  getDataFromLocalStorage: () => {
    const dataJSON = localStorage.getItem(CART);
    let cartArray = [];
    if (dataJSON !== null) {
      cartArray = JSON.parse(dataJSON);
    }
    return cartArray;
  },

  renderCart: (cart) => {
    let contentHTML = "";
    let total = 0;
    let totalProduct = 0;

    cart.map((item) => {
      let itemHTML = `
    <tr class="d-flex flex-wrap mb-5">
        <td class="text-left" style="width: 15%">
          <img
            src=${item.img}
            alt=""
            style="width: 100%; height: 3rem; object-fit: contain"
          />
        </td>
        
        <td class="pl-3 text-left" style="width: 40%">
        ${item.name.length <= 16 ? item.name : item.name.slice(0, 15) + "..."}
        </td>
        
        <td style="width: 20%">
          <button class="btn btn-light p-0">
            <i 
            class="fa fa-angle-left" 
            onclick="changeQuantity(${item.id}, -1)"
            ></i>
          </button>
          
          <p class="d-inline-block mb-0" style="width: 35%">${item.quantity}</p>
        
          <button class="btn btn-light p-0">
            <i 
            class="fa fa-angle-right" 
            onclick="changeQuantity(${item.id}, 1)"
            ></i>
          </button>
        </td>

        <td style="width: 20%">$${item.price}</td>

        <td class="text-right" style="width: 5%">
          <button 
          class="btn p-0" 
          onclick="deleteProductInCart(${item.id})">
            <i class="fa fa-trash"></i>
          </button>
        </td>
      </tr>
        `;
      contentHTML += itemHTML;
      total += item.price * item.quantity;
      totalProduct += item.quantity;
    });

    let paySection = `
      <p class="text-right">Total: $${new Intl.NumberFormat("de-DE").format(
        total
      )}</p>
  
      <div class="d-flex justify-content-end">
        <button 
        class="btn btn-light" 
        data-dismiss="modal"
        onclick="purchase()">
          
          Purchase
          <i class="fa fa-money-check ml-2"></i>
        
        </button>
  
        <button class="btn btn-light ml-3" onclick="clearCart()">
          Clear Cart
          <i class="fa fa-trash ml-2"></i>
        </button>
  </div>
`;
    document.getElementById("header-cart-quantity").textContent = totalProduct;
    document.getElementById("cart-table").innerHTML = contentHTML;
    document.getElementById("payment-section").innerHTML = paySection;

    if (cart.length === 0) {
      document.getElementById(
        "payment-section"
      ).innerHTML = `<p class="display-4 mb-5">You Haven't Added Any Product In To Cart</p>`;
    }
  },

  addProductToCart: (productList, id, cart) => {
    const cloneProductList = [...productList];
    let index = cloneProductList.findIndex((item) => {
      return item.id * 1 === id;
    });

    let product = { ...cloneProductList[index], quantity: 1 };

    let cartIndex = cart.findIndex((item) => {
      return item.id === product.id;
    });

    cartIndex === -1 ? cart.push(product) : (cart[cartIndex].quantity += 1);
  },

  deleteProductInCart: (cart, id) => {
    let index = cart.findIndex((item) => {
      return item.id == id;
    });

    if (index !== -1) {
      cart.splice(index, 1);
    }
  },

  changeQuantity: (cart, id, step) => {
    let index = cart.findIndex((item) => {
      return item.id == id;
    });

    if (index !== -1) {
      cart[index].quantity += step;
    }

    if (cart[index].quantity <= 0) {
      cart.splice(index, 1);
    }
  },

  clearCart: (cart) => {
    cart.splice(0, cart.length);
  },
};
