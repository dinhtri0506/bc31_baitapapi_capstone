import { DOM_MODELS } from "./dom_models.js";

export const PRODUCT_MODELS = {
  newProduct: (romValue = "") => {
    const elementArray = DOM_MODELS.addNewProductModal();

    const getBrandElementAttribute =
      elementArray[0].options[elementArray[0].selectedIndex];

    const product = {
      brand: elementArray[0].value,

      brandLogoImage: getBrandElementAttribute.getAttribute("brandLogoImage"),

      brandContainerBackground: getBrandElementAttribute.getAttribute(
        "brandContainerBackground"
      ),

      name: elementArray[1].value,
      img: elementArray[2].value,
      price: elementArray[3].value,
      ram: elementArray[4].value,
      rom: romValue,
      display: elementArray[6].value,
    };
    return product;
  },
  emptyDataProduct: {
    brand: "",
    name: "",
    img: "",
    price: "",
    ram: "",
    rom: "",
    display: "",
  },
};
