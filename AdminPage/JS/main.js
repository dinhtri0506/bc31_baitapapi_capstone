import { DOM_CONTROLLER } from "./controllers/dom_controllers.js";
import { MODAL_CONTROLLERS } from "./controllers/modal_controllers.js";
import { TABLE_CONTROLLERS } from "./controllers/table_controllers.js";
import { VALIDATION_CONTROLLERS } from "./controllers/validation_controllers.js";
import { ProductModalBody } from "./layout/productModal/ProductModalBody.js";
import { PRODUCT_MODELS } from "./models/product_models.js";
import { DATA_SERVICES } from "./services/data_services.js";
import { LOADING_SERVICES } from "./services/loading_services.js";

document.getElementById("product-modal-body").innerHTML = ProductModalBody;

let PhoneArray = [];
let updateId = null;

let romValue = "";
let getRomValue = (value) => {
  let rom = DOM_CONTROLLER.getValueOnClick(value);
  romValue = rom;
};
window.getRomValue = getRomValue;

let resetModalForm = () => {
  let emptyData = PRODUCT_MODELS.emptyDataProduct;
  MODAL_CONTROLLERS.setDataForModalForm(emptyData);
};
window.resetModalForm = resetModalForm;

let renderTable = () => {
  LOADING_SERVICES.loadingOn();
  DATA_SERVICES.getData()
    .then((success) => {
      PhoneArray = success.data;
      TABLE_CONTROLLERS.renderPhoneList(PhoneArray);
      LOADING_SERVICES.loadingOff();
    })
    .catch((error) => {
      LOADING_SERVICES.loadingOff;
    });
};
renderTable();

let addNewProduct = () => {
  let product = PRODUCT_MODELS.newProduct(romValue);

  if (VALIDATION_CONTROLLERS.productInfoValidate(product)) {
    LOADING_SERVICES.loadingOn();
    DATA_SERVICES.addProduct(product)
      .then((success) => {
        resetModalForm();
        renderTable();
      })
      .catch((error) => {
        LOADING_SERVICES.loadingOff();
      });
  }
};
window.addNewProduct = addNewProduct;

let deleteProduct = (id) => {
  LOADING_SERVICES.loadingOn();
  DATA_SERVICES.deleteProduct(id)
    .then((success) => {
      renderTable();
    })
    .catch((error) => {
      LOADING_SERVICES.loadingOff();
    });
};
window.deleteProduct = deleteProduct;

let putDataBackToModalForm = (productId) => {
  updateId = productId;
  let index = PhoneArray.findIndex((item) => {
    return item.id * 1 === productId;
  });
  let product = PhoneArray[index];
  romValue = product.rom;
  MODAL_CONTROLLERS.setDataForModalForm(product);
};
window.putDataBackToModalForm = putDataBackToModalForm;

let updateProductInfo = () => {
  if (updateId === null) {
    return alert("Select product you want to update first!");
  }
  let product = PRODUCT_MODELS.newProduct(romValue);
  let newProduct = { ...product, id: updateId };
  if (VALIDATION_CONTROLLERS.productInfoValidate(product)) {
    LOADING_SERVICES.loadingOn();
    DATA_SERVICES.updateProduct(newProduct)
      .then((success) => {
        renderTable();
        resetModalForm();
        updateId = null;
      })
      .catch((error) => {
        LOADING_SERVICES.loadingOff();
      });
  }
};
window.updateProductInfo = updateProductInfo;
