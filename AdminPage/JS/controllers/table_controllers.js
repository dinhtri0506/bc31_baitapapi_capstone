export const TABLE_CONTROLLERS = {
  renderPhoneList: (phoneArray) => {
    let contentHTML = "";

    let clonePhoneArray = JSON.parse(JSON.stringify(phoneArray));
    clonePhoneArray.forEach((item) => {
      item.price * 1 === 0 ? (item.price = "updating...") : (item.price += "$");
      item.ram * 1 === 0 ? (item.ram = "updating...") : (item.ram += "GB");
      item.rom * 1 === 0 ? (item.rom = "updating...") : (item.rom = item.rom);
    });

    clonePhoneArray.map((item) => {
      let contentRendered = `<tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>
              <img
                src=${item.img}
                alt=""
              />
            </td>
            <td>${item.price}</td>
            <td>${item.ram} / ${item.rom}</td>
            <td>
              <div>
                <button class="btn btn-warning mr-2" 
                data-toggle="modal"
                data-target="#productModal"
                onclick="putDataBackToModalForm(${item.id})">
                  <i class="fa fa-sync-alt"></i>
                </button>
                <button class="btn btn-danger ml-2" onclick="deleteProduct(${item.id})">
                  <i class="fa fa-trash-alt"></i>
                </button>
              </div>
            </td>
          </tr>`;
      contentHTML += contentRendered;
    });
    document.getElementById("phone-table").innerHTML = contentHTML;
  },
};
