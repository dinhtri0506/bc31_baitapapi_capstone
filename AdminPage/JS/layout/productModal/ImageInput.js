export const ImageInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Image</span>
    </div>
    
    <input type="text" id="product-image" class="form-control" />
</div>
            
<span
id="image-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;
