const ROMArray = ["64GB", "128GB", "256GB", "512GB", "1TB", ""];
let contentHTML = "";
ROMArray.map((item) => {
  const radioButton = `
    <div class="form-check form-check-inline col-4 m-0 p-0">
            
        <input
        class="form-check-input"
        type="radio"
        name="rom-measure-radio"
        value="${item}"
        onclick="getRomValue(this.value)"
        ${item !== "" ? "" : "checked"}
        />
      
        <label class="form-check-label">
        ${item !== "" ? (item = item) : (item = "Unknown")}
        </label>
                
    </div>
    `;
  contentHTML += radioButton;
});

export const RomInput = `
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="rom">ROM</label>
    </div>
    
    <div class="form-control h-100">
        
        <div class="w-100 row px-3">
           ${contentHTML}   
        </div>

    </div>
</div>
`;
