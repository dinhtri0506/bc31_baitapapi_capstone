export const PriceInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Price</span>
    </div>

    <input type="text" id="price" class="form-control" />

    <div class="input-group-append">
        <span class="input-group-text">$</span>
    </div>
</div>

<span
id="price-notice"
class="d-inline-block mb-3 text-danger"
></span>
`;
